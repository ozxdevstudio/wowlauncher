﻿namespace wowlauncher.XAMLStyles
{
    using System;
    using System.Windows.Controls;

    public partial class CustomProgressBar : UserControl
    {
        private int _currentValue;
        private int _lineMaxSize = 590;
        private int _maxValue = 1;
        private int _minValue = 1;
        private string _text;

        public CustomProgressBar()
        {
            InitializeComponent();
        }

        public void SetMin(int min)
        {
            this._maxValue = min;
        }

        public void SetMax(int max)
        {
            this._maxValue = max;
        }

        public void SetCurrent(int max)
        {
            this._currentValue = max;
        }

        public void ProgressPlus()
        {
            if (this._currentValue < this._maxValue)
            {
                this._currentValue++;
                this.UpdateText();
            }
        }

        public bool IsFinished()
        {
            return this._currentValue == this._maxValue;
        }

        public void SetText(string text)
        {
            this._text = text;
        }

        public void Update()
        {
            this.UpdateText();
        }

        public void Draw(string text, int curVal, int maxVal)
        {
            this.BarText.Content = String.Format("{0} - {1} / {2} %", text, curVal, maxVal);
            this.progressLine.Width = (curVal / (float)maxVal) * this._lineMaxSize;
        }

        public void Draw(string text)
        {
            this.BarText.Content = text;
            this.progressLine.Width = this._lineMaxSize;
        }

        private void UpdateText()
        {
            this.BarText.Content = String.Format("{0} - {1} / {2}", this._text, this._currentValue, this._maxValue);
            this.progressLine.Width = (this._currentValue / (float)this._maxValue) * this._lineMaxSize;
        }

        public void Clear()
        {
            this._currentValue = 0;
            this._maxValue = 0;
            this.BarText.Content = "";
        }

        public void DrawTextOnly()
        {
            this.BarText.Content = this._text;
            this.progressLine.Width = this._lineMaxSize;
        }
    }
}