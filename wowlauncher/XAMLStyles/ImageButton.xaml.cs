﻿namespace wowlauncher.XAMLStyles
{
    using System.Windows.Controls;
    using System.Windows.Media;

    public partial class ImageButton : UserControl
    {
        public ImageButton()
        {
            InitializeComponent();
        }

        public ImageSource OnEnableImageSource { get; set; }
        public ImageSource OnHoverImageSource { get; set; }
        public ImageSource OnDisableImageSource { get; set; }
        public ImageSource OnleaveImageSource { get; set; }

        public void HoverAction()
        {
            this.ImgBrush.ImageSource = this.OnHoverImageSource;
        }

        public void EnableAction()
        {
            this.ImgBrush.ImageSource = this.OnEnableImageSource;
        }

        public void DisableAction()
        {
            this.ImgBrush.ImageSource = this.OnDisableImageSource;
        }

        public void LeaveAction()
        {
            this.ImgBrush.ImageSource = this.OnleaveImageSource;
        }

        private void UserControl_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.HoverAction();
        }

        private void UserControl_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.LeaveAction();
        }

        private void UserControl_IsEnabledChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if ((bool)(e.NewValue))
            {
                this.EnableAction();
            }
            else
            {
                this.DisableAction();
            }
        }
    }
}