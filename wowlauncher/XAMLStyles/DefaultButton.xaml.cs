﻿namespace wowlauncher.XAMLStyles
{
    using System.Windows.Controls;

    /// <summary>
    ///     Логика взаимодействия для DefaultButton.xaml
    /// </summary>
    public partial class DefaultButton : UserControl
    {
        public DefaultButton()
        {
            InitializeComponent();
        }

        public double ContentFontSize { get; set; }

        public void Update()
        {
            this.ButtonText.FontSize = this.ContentFontSize;
        }

        public void SetText(string text)
        {
            this.ButtonText.Content = text;
        }
    }
}