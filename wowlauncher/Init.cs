﻿namespace wowlauncher
{
    using System.Windows.Forms;
    using wowlauncher.Systems;
    using wowlauncher.Windows;

    public class Init : ApplicationContext
    {
        public Init()
        {
            var window = WindowController.RegisterWindow<MainWindow>("MainWindow");
            window.Show();
        }

        public static void Exit()
        {
            Application.Exit();
        }
    }
}