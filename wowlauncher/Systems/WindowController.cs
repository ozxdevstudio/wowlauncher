﻿namespace wowlauncher.Systems
{
    using System.Collections.Generic;
    using System.Linq;
    using wowlauncher.Interfaces;
    using wowlauncher.Windows;

    public static class WindowController
    {
        private static List<IWindow> _windows = new List<IWindow>();

        public static TWindow RegisterWindow<TWindow>(string name) where TWindow : IWindow, new()
        {
            var newWindow = _windows.FirstOrDefault(w => w.Name == name);
            if (newWindow == null)
            {
                newWindow = new TWindow { Name = name };
                _windows.Add(newWindow);
            }
            if (!(newWindow is MainWindow))
            {
                newWindow.Trey();
            }
            return (TWindow)newWindow;
        }

        public static TWindow GetWindow<TWindow>(string name) where TWindow : IWindow, new()
        {
            var window = RegisterWindow<TWindow>(name);
            return window;
        }
    }
}