﻿namespace wowlauncher.Systems
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using wowlauncher.Entities;

    public static class UpdateController
    {
        public static LauncherTaskList<string, string> FileTaskList = new LauncherTaskList<string, string>();
        public static WebDownloader client = new WebDownloader();
        public static bool Downloading = false;
        public static int Position = 0;


        public static bool CheckUpdate()
        {
            var answer = false;
            if (LauncherConfig.CustomPatches)
            {
                var path = LauncherConfig.AppDir + LauncherConfig.PatchListFileName;
                var loadMsg = "Загрузка списка обновлений";
                var endMsg = loadMsg + " - завершена.";
                LoadFile(path, new Uri(LauncherConfig.PatchListFileUri), loadMsg, endMsg);
                using (var reader = new StreamReader(File.OpenRead(path)))
                {
                    var i = Convert.ToInt32(reader.ReadLine());
                    for (; i > 0; i--)
                    {
                        var strs = reader.ReadLine().Split(' ');
                        if (strs.Count() == 2)
                        {
                            FileTaskList.AddItem(strs[1], strs[0]);
                        }
                    }
                }
                path = LauncherConfig.AppDir + LauncherConfig.MPQPatchesPath;
                if (Directory.Exists(path))
                {
                    // Throw error message
                }
                foreach (var task in FileTaskList)
                {
                    path = LauncherConfig.AppDir + LauncherConfig.MPQPatchesPath + task.Value;
                    var md5hash = MD5.Create();
                    if (!File.Exists(path))
                    {
                        if (MD5Hash.VerifyMd5Hash(md5hash, task.Value + LauncherConfig.Salt, task.Key))
                        {
                            answer = true;
                        }
                        else
                        {
                            task.IsEnd = true;
                        }
                    }
                }
            }
            var list = FileTaskList.Where(tl => !tl.IsEnd).ToList();
            FileTaskList.Clear();
            FileTaskList.AddRange(list);
            for (int i = 0; i < FileTaskList.Count; i++)
            {
                FileTaskList[i].Prioritet = i;
            }
            return answer;
        }

        public static void LoadPatches(LauncherTask<string, string> launcherTask)
        {
            WebDownloader client = new WebDownloader();
            var path = LauncherConfig.AppDir + LauncherConfig.MPQPatchesPath + launcherTask.Value;
            var md5hash = MD5.Create();
            if (!File.Exists(path))
            {
                if (MD5Hash.VerifyMd5Hash(md5hash, launcherTask.Value + LauncherConfig.Salt, launcherTask.Key))
                {
                    var loadMsg = "Загрузка " + launcherTask.Value;
                    var endMsg = loadMsg + " - завершена.";
                    LoadFileAsync(path, new Uri(LauncherConfig.BaseUri + launcherTask.Value), loadMsg, endMsg,
                        launcherTask);
                }
            }
        }

        public static void LoadFile(string path, Uri uri, string messageOnLoad, string messageOnEndLoad)
        {
            var client = Download(path, uri, messageOnLoad, messageOnEndLoad);
            while (client.IsBusy)
            {
            }
        }

        public static WebDownloader LoadFileAsync(string path, Uri uri, string messageOnLoad, string messageOnEndLoad,
            LauncherTask<string, string> launcherTask)
        {
            return Download(path, uri, messageOnLoad, messageOnEndLoad, launcherTask);
        }

        private static WebDownloader Download(string path, Uri uri, string messageOnLoad, string messageOnEndLoad,
            LauncherTask<string, string> launcherTask)
        {
            client.DownloadFileAsync(uri, path, messageOnLoad, messageOnEndLoad, launcherTask);
            return client;
        }

        private static WebDownloader Download(string path, Uri uri, string messageOnLoad, string messageOnEndLoad)
        {
            client.DownloadFileAsync(uri, path, messageOnLoad, messageOnEndLoad);
            return client;
        }

        public static void DoUpdate(int position)
        {
            LoadPatches(FileTaskList[position]);
        }

        /*
        public static void DoUpdate(object sender, EventArgs e)
        {
            foreach (var launcherTask in FileTaskList)
            {
                while (Downloading)
                {
                }
                Downloading = true;
                StartDownloadingAsync(launcherTask).Wait();
            }
        }*/

        public static async Task StartDownloadingAsync(LauncherTask<string, string> launcherTask)
        {
            var client = new WebDownloader();
            var path = LauncherConfig.AppDir + LauncherConfig.MPQPatchesPath + launcherTask.Value;
            var md5hash = MD5.Create();
            if (!File.Exists(path))
            {
                if (MD5Hash.VerifyMd5Hash(md5hash, launcherTask.Value + LauncherConfig.Salt, launcherTask.Key))
                {
                    var loadMsg = "Загрузка " + launcherTask.Value;
                    var endMsg = loadMsg + " - завершена.";
                    LoadFileAsync(path, new Uri(LauncherConfig.BaseUri + launcherTask.Value), loadMsg, endMsg,
                        launcherTask);
                }
            }
        }
    }
}