﻿namespace wowlauncher.Systems
{
    using System.Windows.Forms;

    public static class LauncherConfig
    {
        public static string AppDir = Application.StartupPath + "/";
        public static string WOWFilePath = "wow.exe";
        public static bool CustomPatches = true;
        public static bool CustomWOWFile = true;
        public static string MPQPatchesPath = "/Data/";
        public static string PatchListFileName = "patch_list.txt";
        public static string BaseUri = "http://localhost/wowlauncher/";
        public static string PatchListFileUri = BaseUri + PatchListFileName;
        public static string WOWFileName = "wow.exe";
        public static string WOWFileUri = BaseUri + WOWFileName;
        public static string Salt = "salt";
    }
}