﻿namespace wowlauncher.Entities
{
    using System;
    using System.ComponentModel;
    using System.Net;
    using System.Windows.Threading;
    using wowlauncher.Systems;
    using wowlauncher.Windows;

    public class WebDownloader : WebClient
    {
        public LauncherTask<string, string> DownloadTask = new LauncherTask<string, string>();
        public string EndMessage = String.Empty;
        public string LoadMessage = String.Empty;

        protected override void OnDownloadProgressChanged(DownloadProgressChangedEventArgs e)
        {
            base.OnDownloadProgressChanged(e);
            var window = WindowController.GetWindow<MainWindow>("MainWindow");
            window.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                new Action(() => window.UpdateProgressBar(e.ProgressPercentage, 100, this.LoadMessage)));
        }

        protected override void OnDownloadFileCompleted(AsyncCompletedEventArgs e)
        {
            base.OnDownloadFileCompleted(e);
            var window = WindowController.GetWindow<MainWindow>("MainWindow");
            window.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                new Action(() => window.UpdateProgressBar(100, 100, this.EndMessage)));
            this.DownloadTask.IsEnd = true;
            UpdateController.Position++;
            UpdateController.Downloading = false;
        }

        public void DownloadFileAsync(Uri uri, string path, string messageOnLoad, string messageOnEndLoad)
        {
            this.LoadMessage = messageOnLoad;
            this.EndMessage = messageOnEndLoad;
            this.DownloadFileAsync(uri, path);
        }

        public void DownloadFile(string path, Uri uri, string messageOnLoad, string messageOnEndLoad)
        {
            this.LoadMessage = messageOnLoad;
            this.EndMessage = messageOnEndLoad;
            this.DownloadFile(uri, path);
        }

        internal void DownloadFileAsync(Uri uri, string path, string messageOnLoad, string messageOnEndLoad,
            LauncherTask<string, string> launcherTask)
        {
            this.LoadMessage = messageOnLoad;
            this.DownloadTask = new LauncherTask<string, string>(launcherTask.Key, launcherTask.Value)
            {
                Prioritet = launcherTask.Prioritet
            };
            this.EndMessage = messageOnEndLoad;
            this.DownloadFileAsync(uri, path);
        }
    }
}