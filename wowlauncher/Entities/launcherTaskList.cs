﻿namespace wowlauncher.Entities
{
    using System.Collections.Generic;

    public class LauncherTask<TKey, TValue>
    {
        public bool IsEnd;

        public int Prioritet = 0;

        public LauncherTask()
        {
        }

        public LauncherTask(TKey key, TValue value)
        {
            this.Key = key;
            this.Value = value;
        }

        public TKey Key { get; set; }
        public TValue Value { get; set; }
    }

    public class LauncherTaskList<TKey, TValue> : List<LauncherTask<TKey, TValue>>
    {
        public void AddItem(TKey key, TValue value)
        {
            this.Add(new LauncherTask<TKey, TValue>(key, value));
        }

        public void AddItem(TKey key, TValue value, bool isEnd)
        {
            this.Add(new LauncherTask<TKey, TValue>(key, value) { IsEnd = isEnd });
        }
    }
}