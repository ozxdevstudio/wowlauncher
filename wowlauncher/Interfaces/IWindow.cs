﻿namespace wowlauncher.Interfaces
{
    public interface IWindow
    {
        string Name { get; set; }
        void Open();
        void Trey();
    }
}