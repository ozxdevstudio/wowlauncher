﻿namespace wowlauncher.Windows
{
    using System;
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Threading;
    using wowlauncher.Interfaces;
    using wowlauncher.Systems;
    using static wowlauncher.Systems.UpdateController;

    public partial class MainWindow : Window, IWindow
    {
        private Timer _timer = new Timer();

        public MainWindow()
        {
            InitializeComponent();
            this.cProgressBar.Clear();
            this.cProgressBar.SetMax(100);
            this.EnterButton.EnableAction();
            this.CloseButton.EnableAction();
            this.MinimButton.EnableAction();
            _timer.Interval = 1000;
            _timer.Tick += DoCheckFileUpdate;
        }

        public void Open()
        {
            this.Show();
        }

        public void Trey()
        {
            this.Hide();
        }

        private void DoCheckFileUpdate(object sender, EventArgs e)
        {
            if (!Downloading)
            {
                Downloading = true;
                this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<int>(DoUpdate), Position);
            }
            if (Position == FileTaskList.Count)
            {
                _timer.Stop();
            }
        }

        public DialogResult OpenDialog()
        {
            return this.OpenDialog();
        }

        private void Rectangle_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void CloseButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Init.Exit();
        }

        private void MinimButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            this.EnterButton.IsEnabled = false;
            if (CheckUpdate())
            {
                var msg = WindowController.GetWindow<MessageWindow>("MsgWindow");
                var answer =
                    (bool)msg.WithMessage("Требуется обновление, обновить?").WithButton("Download").OpenDialog();
                if (answer)
                {
                    Position = 0;
                    _timer.Start();
                }
            }
            else
            {
                this.EnterButton.IsEnabled = true;
            }
        }

        public void UpdateProgressBar(int cur, int max, string text)
        {
            if (cur == max)
            {
                this.EnterButton.IsEnabled = true;
                this.cProgressBar.Draw(text);
            }
            else
            {
                this.cProgressBar.Draw(text, cur, max);
            }
        }
    }
}