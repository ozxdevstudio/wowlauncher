﻿namespace wowlauncher.Windows
{
    using System.Windows;
    using wowlauncher.Interfaces;

    /// <summary>
    ///     Логика взаимодействия для MessageWindow.xaml
    /// </summary>
    public partial class MessageWindow : Window, IWindow
    {
        private bool _dialog;

        public MessageWindow()
        {
            InitializeComponent();
            this.CloseButton.EnableAction();
        }

        public void Open()
        {
            this._dialog = false;
            this.Show();
        }

        public void Trey()
        {
            this.Hide();
        }

        public MessageWindow WithMessage(string text)
        {
            this.Messagetext.Content = text;
            return this;
        }

        public MessageWindow WithButton(string text)
        {
            this.ButtonOK.SetText(text);
            return this;
        }

        public bool? OpenDialog()
        {
            this._dialog = true;
            var result = this.ShowDialog();
            return result;
        }

        private void ButtonOK_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this._dialog)
            {
                this.DialogResult = true;
            }
            this.Close();
        }

        private void CloseButton_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this._dialog)
            {
                this.DialogResult = false;
            }
            this.Close();
        }
    }
}